require_relative 'lib/simple_ruby_gem/version'

Gem::Specification.new do |spec|
  spec.name          = "simple_ruby_gem"
  spec.version       = SimpleRubyGem::VERSION
  spec.authors       = ["j_lar"]
  spec.email         = ["jlarsen@gitlab.com"]

  spec.summary       = %q{Simple Ruby gem}
  spec.description   = %q{Simple Ruby gem}
  spec.homepage      = "http://gdk.test:3000/root/simple_ruby_gem"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "http://gdk.test:3000/root/simple_ruby_gem"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
